import React, {Component} from 'react';
import { StyleSheet, Text, View, ScrollView, Navigator, Button, SectionList, List, ListItem, Image, TouchableOpacity} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
//import {List, ListItem, Image} from 'react-native-elements'


const jsonUrl = 'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

// async function getDatas(){
//     const res = await fetch(jsonUrl);
//     const data = await res.json();
//     console.log(data[0].id)
//     return data
// };
//keyy = 3;
// console.log("data")
// //eeel = getDatas();
// //console.log(eeel[0].id)

// // dataF = getDatas();
// // })
// // getDatas();



/* Style */ 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mystyle: {
      color: 'blue',
      fontWeight: 'bold',
  },
  mystyle2: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  mystyleimg: {
      position: 'absolute',
      top: 20,
      bottom: 0,
      left: 0,
      right: 0,
      resizeMode: 'cover',
      //resizeMode: 'stretch'
      //height: '100%',
  }
      
});



/* Home Screen */  
class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state ={ title: 'Gallery', data: [] };
    }

    componentDidMount = async() => {
        const response = await fetch(jsonUrl);
        const data = await response.json() 
        //console.log(data)
        this.setState({data})
    }


    render() {
      //console.log("state", this.state);
      const dataR = Array.from(this.state.data);
      //const {title, dataR} = this.state

      return (
        <View 
        style={styles.mystyle}
        >
          <Text>HomeScreen </Text>
          <ScrollView>
          {/*<Text>{keyy}</Text>
                  
          <Button
            title="Details Screen"
            onPress={() => this.props.navigation.navigate('Details', {
              link: "https://images.unsplash.com/photo-1571942794153-c66861c8d1a9?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjMyNDU2fQ"
            })}
          />
          */}

          {dataR.map((item) => (
          <View key = {'view' + item.id}>
          <Text key={item.id} >
          
          {item.user.name} 
          {item.description} 
          {item.alt_description}
              
          </Text>
          <TouchableOpacity
            key = {'op' + item.id}
            onPress={() => this.props.navigation.navigate('Details', {
              link: item.urls.full
              }
            )}
            >
              <Image
                key = {'img' + item.id}
                source={{ uri: item.urls.thumb }}
                style={{ width: 100, height: 100 }}
              />
          </TouchableOpacity>
        </View>
        ))}

          </ScrollView>
        </View> 
      ) 
      
    }
  }


/* Screen for big img */  
class DetailsScreen extends Component {
  render() {
    const { navigation } = this.props;
    const link = navigation.getParam('link');  
    return (
      <View
      style={{ flex: 1 }}>
        <Text>
        DetailsScreen</Text>

        <Image 
            style={styles.mystyleimg}
            source={{uri: link}}
        />
      </View>
    );
  }
}

/* Routes between screen */
const roots = createStackNavigator(
  {
    Home: HomeScreen,           //home route
    Details: DetailsScreen,     //rout for big img
  },
  {
    initialRouteName: 'Home',
  }
);




const AppContainer = createAppContainer(roots);
  
export default class App extends Component {
       
  render() {
    return <AppContainer />;
  }
}


